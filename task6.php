<html>

<head>
    <style>
        tr.even {
            background-color: lightgreen;
        }

        tr.odd {
            background-color: #BADA55;
        }
    </style>
</head>

<body>
<h1>Daugybos lentelė </h1>
    <table border="1">
        <tbody>
        <?php for ($i = 1; $i <= 10; $i++) {?>
            <tr class="<?php echo $i % 2 == 0 ? 'even' : 'odd' ?>">
            <?php for ($j = 1; $j <= 10; $j++) {?>
                <td>
                    <?php echo "$i * $j = " . $i * $j ?>
                </td>
            <?php }?>
            </tr>
        <?php }?>
        </tbody>
    </table>
</body>

</html>