<?php

$handle = fopen("file.txt", "r");
$lineNumber = 1;
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        if ($line === "\n") {
            continue;
        }

        echo "$lineNumber $line<br>";
        if ($line == "exit\n") {
            break;
        }

        $lineNumber++;

    }

    fclose($handle);
} else {

}
