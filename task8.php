<?php

const PLAYER_1_NAME = 'Petras';
const PLAYER_2_NAME = 'Kazys';

$player1Score = 0;
$player2Score = 0;

$player1TotalScore = 0;
$player2TotalScore = 0;

while ($player1TotalScore < 222 && $player2TotalScore < 222) {
    $player1Score = mt_rand(10, 20);
    $player2Score = mt_rand(5, 25);

    $scoreText = "$player1Score:$player2Score";
    $player1TotalScore += $player1Score;
    $player2TotalScore += $player2Score;

    $totalScoreText = "$player1TotalScore:$player2TotalScore";
    if ($player1Score === $player2Score) {
        echo "Lygiosios $scoreText. Viso rezultatas $totalScoreText<br>";
    } else {
        $winnerName = $player1Score > $player2Score ? PLAYER_1_NAME : PLAYER_2_NAME;
        echo "Partiją laimėjo $winnerName $scoreText. Viso rezultatas $totalScoreText<br>";
    }
}

echo '<br>';

if ($player1TotalScore === $player2TotalScore) {
    echo "Lygiosios. Viso rezultatas $totalScoreText<br>";
} else {
    $winnerName = $player1TotalScore > $player2TotalScore ? PLAYER_1_NAME : PLAYER_2_NAME;
    echo "Žaidimą laimėjo $winnerName. Viso rezultatas $totalScoreText<br>";
}
