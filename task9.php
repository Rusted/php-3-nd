<?php

for ($i = 1; $i <= 100; $i++) {
    if ($i % 5 === 0 && $i % 7 === 0) {
        echo 'Fizz Buzz';
    } elseif ($i % 5 === 0) {
        echo 'Fizz';
    } elseif ($i % 7 === 0) {
        echo 'Buzz';
    } else {
        echo $i;
    }

    if ($i === 100) {
        echo '.';
    } else {
        echo ', ';
    }
}
